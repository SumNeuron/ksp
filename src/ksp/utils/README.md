# Utils
this sub-modules can be expanded to have any helpers needed.
Currently it consists of the submodule `config` which notably stores two
__global__ variables:

- `INFINITY` (defaults to `10000`): an arc with this cost signifies that it has been removed from the graph, and
- `UNDEFINED` (defaults to `None`): represents no predecessor
