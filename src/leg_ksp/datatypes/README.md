# DataTypes

Herein are two datatypes for convenience:

- a priority queue to be used in Dijkstra's algorithm based on Eppestein's
[recipe][PriorityDictionary], and

- AdjacencyDict, which is an extension of the base `dict` class.

Understanding the priority queue at the linked [recipe][PriorityDictionary]
is not required to utilize this package. However, understanding the `AdjacencyDict`
is.

The `AdjacencyDict` takes data in the form of:

```
data =
{
  'vertex_0': {
    'vertex_i': cost_of_edge_0i,
    'vertex_j': cost_of_edge_0j,
    ...
  },

  ...,

  'vertex_i': {
    'vertex_k': cost_of_edge_ik,
    ...
  },
  ...
}
```

whereby the keys of outer dictionary, `data`, are the vertices of the graph.
the values of `data[key]` is itself a vanilla `python` dictionary (`dict`).
This inner dictionary has `subkey: value` pairs whereby the `subkey` is a vertex name
in the graph (should exist in `data`) indicating an _arc_ (directed edge) from
`key` to `subkey` and `value` is the cost associated with the _arc_.

Given the above example, an `AdjacencyDict` can be initialized by calling:

```
g = AdjacencyDict(data)
```

in other words, `arc_ij` with `cost_k` is represented as a nested dictionary
`dict[i][j] = k`. The need for the custom class comes from the algorithms `dijkstra` and `yen`
- as implemented here - as they assume that the passed
graph is an `AdjacencyDict` which has some associated methods for facilitating the algorithms.





[PriorityDictionary]: http://code.activestate.com/recipes/117228/
