# README
Based on [**@Pent00**][Pent00]'s implementation of [Yen's K-Shortest Path][PUB:yen].

Notable differences:

- `Python3` support
- cleaned up classes and file structure
- removed dependency on `GraphViz`
- removed loading data from `/data/json` and added support to dump a `dict` into
the graph class.

See `jupyter` subdirectory for a notebook demonstrating how to use


[Pent00]: https://github.com/Pent00/YenKSP
 <!-- Kevin R <KRPent@gmail.com> -->
[PUB:yen]: https://pubsonline.informs.org/doi/abs/10.1287/mnsc.17.11.712
