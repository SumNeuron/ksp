# Algorithms:

[Yen's][PUB:yen] algorithm is a modification of [Eppstein's][PUB:epp] algorithm
for solving the [K-Shortest Paths (KSP) problem][WIKI:ksp]. Both [Yen's][PUB:yen]
and [Eppstein's][PUB:epp] algorithms are dependent on Dijkstra's. For convenience
the wikipedia summaries are linked here: [yen][WIKI:yen], [epp][WIKI:epp], [ksp][WIKI:ksp].

Of note is the Priority Queue used in Dijkstra's algorithm, which is implemented
under `datatypes/priority_dict` based on Eppestein's [recipe][PriorityDictionary].

Since this serves as a lightweight KSP package, the only algorithms to be found
here are: `dijkstra`, `path` (reconstructs the pathway from `source` to `target`),
and `yen`.


[WIKI:yen]: https://en.wikipedia.org/wiki/Yen%27s_algorithm
[WIKI:epp]: https://en.wikipedia.org/wiki/K_shortest_path_routing#Algorithm
[WIKI:ksp]: https://en.wikipedia.org/wiki/K_shortest_path_routing
[PUB:epp]: https://epubs.siam.org/doi/10.1137/S0097539795290477
[PUB:yen]: https://pubsonline.informs.org/doi/abs/10.1287/mnsc.17.11.712
[PriorityDictionary]: http://code.activestate.com/recipes/117228/
