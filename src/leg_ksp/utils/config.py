# An edge with this cost signifies that it has been removed from the graph.
# This value implies that any edge in the graph must be very small in
# comparison.
INFINITY = 10000

## Represents a NULL predecessor.
UNDEFINDED = None
