# sub-modules
the module `ksp` (available from `pypi`) has three submodules:

- algorithms
- datatypes
- utils

all methods, parameters and classes are lifted into the `ksp` module.

for more info see the readme files of the corresponding sub-directories.
