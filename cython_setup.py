from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension


files = [
    './src/ksp/algorithms/dijkstra.pyx', './src/ksp/algorithms/utils.pyx', './src/ksp/algorithms/yen.pyx',
    # './cksp/algorithms/__init__.pyx',

    './src/ksp/datatypes/adjacency_dict.pyx', './src/ksp/datatypes/priority_dict.pyx',
    # './cksp/datatypes/__init__.pyx',

    './src/ksp/utils/config.pyx',
    # './cksp/utils/__init__.pyx'

]

# files = [
#     './cksp/__init__.pyx',
#     './cksp/algorithms/dijkstra.pyx', './cksp/algorithms/utils.pyx', './cksp/algorithms/yen.pyx', './cksp/algorithms/__init__.pyx',
#     './cksp/datatypes/adjacency_dict.pyx', './cksp/datatypes/priority_dict.pyx', './cksp/datatypes/__init__.pyx',
#     './cksp/utils/config.pyx', './cksp/utils/__init__.pyx'
# ]

# extensions = [
#     Extension( "algorithms", [
#         './ksp/algorithms/dijkstra.py', './ksp/algorithms/utils.py', './ksp/algorithms/yen.py', './ksp/algorithms/__init__.py'
#     ]),
#     Extension( "datatypes", [
#         './ksp/datatypes/adjacency_dict.py', './ksp/datatypes/priority_dict.py', './ksp/datatypes/__init__.py'
#     ]),
#     Extension( "utils", [
#         './ksp/utils/config.py', './ksp/utils/__init__.py'
#     ]),
# ]


setup(name='ksp', ext_modules=cythonize(files, language_level=3))
