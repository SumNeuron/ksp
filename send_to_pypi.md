```
# make latest version
python setup.py sdist bdist_wheel

# make cython version
python cython_setup.py build_ext --inplace

# submit latest version
python -m twine upload  dist/*<version>
python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*<version>
```


python setup.py sdist bdist_wheel; python cython_setup.py build_ext --inplace;
python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
